package de.favo.mircophone;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;

import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

/*
        Microphone - A microphone App for Android
        Copyright (C) 2018-2019  Felix v. Oertzen
        felix@von-oertzen-berlin.de

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

public class Microphone extends Activity {
    private static final int                 CODE_AUDIO_PERMISSION = 456;
    public  static final String              SHARED_PREFERENCES = "MicrophonePrefs";
    public  static final String              SP_FIRST_TIME = "FirstTimeOpened";
    private              RelativeLayout      mMainLayout;
    private              ImageButton         mRecordImageButton;
    private              ImageButton         mThreePointsImageButton;
    private              TextView            mSampleRateTextView;
    private              BroadcastReceiver   mAudioBroadcastReceiver;
    private              SharedPreferences   mSharedPreferences;
    private              BackgroundService   mServiceBinder;
    private              boolean             isRecording;
    public               ServiceConnection   myConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder binder) {
            mServiceBinder = ((BackgroundService.BackgroundBinder) binder).getService();
            isRecording = mServiceBinder.isRecording();
            toggleRecordingButton(mServiceBinder.isRecording(), false);
        }

        public void onServiceDisconnected(ComponentName className) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_microphone);

        mRecordImageButton = findViewById(R.id.mic_button);
        mThreePointsImageButton = findViewById(R.id.set_button);
        mMainLayout = findViewById(R.id.mic_main);
        mSampleRateTextView = findViewById(R.id.mic_rate);

        mRecordImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRecording = !isRecording;
                toggleRecordingButton(isRecording,true);
            }
        });

        mSharedPreferences = getSharedPreferences(SHARED_PREFERENCES,MODE_PRIVATE);
        mThreePointsImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LegalActivity.class));
            }
        });

        toggleRecordingButton(false, true);

        mAudioBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                toggleRecordingButton(false, true);
            }
        };

        registerReceiver(mAudioBroadcastReceiver,new IntentFilter(Intent.ACTION_HEADSET_PLUG));

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (mSharedPreferences.getBoolean(SP_FIRST_TIME,true))
            mThreePointsImageButton.callOnClick();
        doBindService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mAudioBroadcastReceiver);
    }

    private void toggleRecordingButton(boolean shouldRecord,boolean startRec){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.RECORD_AUDIO) ==
                        PackageManager.PERMISSION_DENIED){
                requestPermissionForMicrophone();
        } else {
            if (shouldRecord) {
                mRecordImageButton.setImageResource(R.mipmap.ic_mic_white_48dp);
                mMainLayout.setBackgroundColor(Color.BLACK);
                if (startRec)
                    startRecording();
            } else {
                mRecordImageButton.setImageResource(R.mipmap.ic_mic_none_white_48dp);
                mMainLayout.setBackgroundColor(Color.DKGRAY);
                if (startRec)
                    stopRecording();
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == CODE_AUDIO_PERMISSION && (!(grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED))) {
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Dialog))
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.text_no_permission)
                    .setNeutralButton(R.string.text_go_to_settings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent openSettingsIntent = new Intent(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri;
                            openSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            uri = Uri.fromParts("package", getPackageName(), null);
                            openSettingsIntent.setData(uri);
                            startActivity(openSettingsIntent);
                        }
                    }).setCancelable(false).show();
        }
    }

    private void requestPermissionForMicrophone(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    CODE_AUDIO_PERMISSION);
        }
    }

    private void startRecording() {
        if (isValidAudioSourceAvailable(this) && mServiceBinder != null)
            mSampleRateTextView.setText(getString(R.string.text_frames,mServiceBinder.startRecording()));
        else
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Dialog))
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.action_plug_in)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            toggleRecordingButton(false, true);
                        }
                    }).setCancelable(false).show();
    }

    private void stopRecording(){
        if (mServiceBinder != null)
            mServiceBinder.stopRecording();
        mSampleRateTextView.setText("");
    }

    static boolean isValidAudioSourceAvailable(Context c){
        AudioManager audioManager = (AudioManager) c.getSystemService(Context.AUDIO_SERVICE);
        assert audioManager != null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            AudioDeviceInfo[] audioDevices = audioManager.getDevices(AudioManager.GET_DEVICES_ALL);
            for (AudioDeviceInfo deviceInfo : audioDevices) {
                if (deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADPHONES
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADSET
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_AUX_LINE
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_HDMI
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_USB_DEVICE
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_USB_HEADSET
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_A2DP
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_HDMI_ARC)
                    return true;
            }
        } else {
            return audioManager.isWiredHeadsetOn() || audioManager.isBluetoothA2dpOn();
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isRecording)
            startService(new Intent(getApplicationContext(), BackgroundService.class));
        unbindService(myConnection);
    }

    public void doBindService() {
        Intent intent = new Intent(this, BackgroundService.class);
        bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
    }
}
