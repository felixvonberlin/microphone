package de.favo.mircophone;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class BackgroundService extends Service {

    /*
        Microphone - A microphone App for Android
        Copyright (C) 2018-2019  Felix v. Oertzen
        felix@von-oertzen-berlin.de

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

    public  static       int                SAMPLE_RATE                 = 44100;
    public  static final int                CHANNELS                    = AudioFormat.CHANNEL_IN_MONO;
    public  static final int                AUDIO_FORMAT                = AudioFormat.ENCODING_PCM_16BIT;
    private static final int                BUFFER                      = 256;
    private        final IBinder            mBinder                     = new BackgroundBinder();
    private              Thread             mRecorderThread;
    private              AudioTrack         mAudioTrack;
    private              AudioRecord        mAudioRecord;
    private              BroadcastReceiver  mAudioBroadcastReceiver;
    private              boolean            isRecording;

    public BackgroundService() {}

    int startRecording(){
        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, CHANNELS, AUDIO_FORMAT, BUFFER);
        try {
            mAudioRecord.startRecording();
        }catch (IllegalStateException ise){
            // The default sample rate is not available on all devices
            SAMPLE_RATE = 8000;
            mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, CHANNELS, AUDIO_FORMAT, BUFFER);
            mAudioRecord.startRecording();
        }

        isRecording = true;

        mRecorderThread = new Thread(new Runnable() {
            public void run() {
                byte[] voice = new byte[BUFFER];
                mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE,
                        AudioFormat.CHANNEL_OUT_MONO,
                        AUDIO_FORMAT, BUFFER,
                        AudioTrack.MODE_STREAM);

                mAudioTrack.play();

                while (isRecording) {
                    try {
                        mAudioRecord.read(voice, 0, BUFFER);
                        mAudioTrack.write(voice, 0, BUFFER);
                    }catch (NullPointerException npe){
                        Log.w("Microphone","NullPointerE.");
                    }
                }
                mAudioTrack.stop();
                mAudioTrack.release();
                mAudioTrack = null;
                mAudioRecord.stop();
                mAudioRecord.release();
                mAudioRecord = null;
            }
        });
        mRecorderThread.start();
        return SAMPLE_RATE;
    }

    void stopRecording(){
        if (isRecording){
            isRecording = false;
        }
        BackgroundNotification.cancel(this);
    }

    public boolean isRecording() {
        return isRecording;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        BackgroundNotification.notify(this);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mAudioBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        mAudioBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                stopRecording();
            }
        };
        registerReceiver(mAudioBroadcastReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        return mBinder;
    }

    class BackgroundBinder extends Binder {
        BackgroundService getService() {
            return BackgroundService.this;
        }
    }

}
